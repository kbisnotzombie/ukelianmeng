CREATE TABLE `agent_bonus` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `agentBonus` double DEFAULT 0,
  `parentAgentBonus` double DEFAULT 0,
  `deleted` tinyint(4) DEFAULT '0',
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
);