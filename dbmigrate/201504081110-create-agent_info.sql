CREATE TABLE `agent_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parentAgentId` bigint(20) DEFAULT NULL,
  `openId` varchar(50) DEFAULT NULL,
  `nickname` varchar(200) default NULL,
  `phone` varchar(50) DEFAULT NULL,
  `disabled` tinyint(4) DEFAULT '0',
  `createTime` datetime DEFAULT NULL,
  `lastModifyTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO agent_info(parentAgentId,openId,nickname,phone,disabled,createTime,lastModifyTime) VALUES('-1','','kb','13581759421',false,NOW(),NOW());