CREATE TABLE `agent_code_order` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `agentId` bigint(20) DEFAULT NULL,
  `orderId` bigint(20) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
);