CREATE TABLE `agent_code_payment_success` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `orderId` bigint(20) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `agentId` bigint(20) DEFAULT NULL,
  `agentBonus` bigint(20) DEFAULT NULL,
  `parentAgentId` bigint(20) DEFAULT NULL,
  `parentAgentBonus` bigint(20) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
);