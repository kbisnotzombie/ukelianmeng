CREATE TABLE `agent_code_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `agentId` bigint(20) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `disabled` tinyint(4) DEFAULT '0',
  `hasUsed` tinyint(4) DEFAULT '0',
  `lastModifyTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
);