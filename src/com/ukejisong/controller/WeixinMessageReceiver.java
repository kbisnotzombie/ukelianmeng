package com.ukejisong.controller;

import java.io.IOException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpMessageHandler;
import me.chanjar.weixin.mp.api.WxMpMessageRouter;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.bean.WxMpXmlOutNewsMessage;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ukejisong.core.utils.RandomUtils;
import com.ukejisong.domain.AgentInfo;
import com.ukejisong.service.AgentCodeInfoService;
import com.ukejisong.service.AgentInfoService;

@Controller
public class WeixinMessageReceiver{
	private static final Logger logger = LoggerFactory.getLogger(WeixinMessageReceiver.class);
	//服务号的微信号，现在写的是测试账号数据
	private static final String wxAccountName = "ukelianmeng";
	private static final String appId = "wxb45d230bef3aea8e";
	private static final String appSecret = "3cf117bc5a4d15ae05c52f845ea31d33";
	private static final String token = "qwertyuiop1234567890asdfghjkl";
	private static final String jsSafeDomain = "http://lianmeng.ukejisong.com";
	
	@Resource(name = "agentInfoService")
	private AgentInfoService agentInfoService;
	
	@Resource(name = "agentCodeInfoService")
	private AgentCodeInfoService agentCodeInfoService;
	
	
	@RequestMapping(value = "/message/weixin", produces="text/plain;charset=UTF-8")
	public @ResponseBody String getMessage(HttpServletRequest request, @RequestParam(required = false) String echostr){
		WxMpInMemoryConfigStorage config = new WxMpInMemoryConfigStorage();
		
		config.setAppId(appId);
		config.setSecret(appSecret);
		config.setToken(token);
		config.setOauth2redirectUri(jsSafeDomain + "/wxredirect");
		
		WxMpServiceImpl wxMpService = new WxMpServiceImpl();
	    wxMpService.setWxMpConfigStorage(config);
	    
		if(!StringUtils.isBlank(echostr)){
			return echostr;
		}
		
		 WxMpMessageHandler handler = new WxMpMessageHandler() {
			@Override
			public WxMpXmlOutMessage handle(WxMpXmlMessage wxMpXmlMessage,
					Map<String, Object> context, WxMpService wxMpService,
					WxSessionManager wxSessionManager) throws WxErrorException {
				
				logger.info(wxMpXmlMessage.toString());
				String lang = "zh_CN"; //语言
				WxMpUser user = wxMpService.userInfo(wxMpXmlMessage.getFromUserName(), lang);

				logger.info(user.toString());
//				logger.info(wxMpXmlMessage.getMsgType() + "~~~~~~" + wxMpXmlMessage.getEvent());
				
				//初次关注公众号
				if(wxMpXmlMessage.getMsgType().equalsIgnoreCase("event") && wxMpXmlMessage.getEvent().equalsIgnoreCase(WxConsts.EVT_SUBSCRIBE)){
					String content = "您好，欢迎关注优课联盟服务号。您目前还不能享用代理权利，请先绑定您的代理账号。回复：“##手机号##”即可绑定代理账号，手机号为成为代理时所使用的手机号。如：##13812345678##。如已经绑定，无需任何操作。";
					return WxMpXmlOutMessage.TEXT()
					  .content(content)
					  .fromUser(wxAccountName)   
					  .toUser(wxMpXmlMessage.getFromUserName())
					  .build();
				}
				
				String openId = user.getOpenId();
				AgentInfo agentInfo = agentInfoService.getAgentInfo(openId);
				//此人还不是代理或者未绑定
				if(agentInfo == null){
					if(wxMpXmlMessage.getMsgType().equalsIgnoreCase("text")){//看输入信息是否是绑定手机号
						String userContent = wxMpXmlMessage.getContent();
						String pattern = "##(\\d{11})##";
						String phone= "";
						Pattern r = Pattern.compile(pattern);
						Matcher m = r.matcher(userContent);
						if(m.find()){//匹配到正确的手机号
							phone = m.group(1);
							
							AgentInfo unbindAgentInfo = agentInfoService.reerieveAgentInfo(phone);
							if(unbindAgentInfo == null){//手机号不是代理
								String wrongPhone = "您好，您的手机号没有代理资格";
								return WxMpXmlOutMessage.TEXT()
										  .content(wrongPhone)
										  .fromUser(wxAccountName)   
										  .toUser(wxMpXmlMessage.getFromUserName())
										  .build();
							}else{
								if(unbindAgentInfo.getOpenId() == null || unbindAgentInfo.getOpenId().equals("")){
									//成功绑定
									agentInfoService.updateOpenId(openId, unbindAgentInfo);
									String success = "您好，已经成功和代理账号绑定";
									return WxMpXmlOutMessage.TEXT()
											  .content(success)
											  .fromUser(wxAccountName)   
											  .toUser(wxMpXmlMessage.getFromUserName())
											  .build();
								}else{
									//该手机号已经绑定过微信
									agentInfoService.updateOpenId(openId, unbindAgentInfo);
									String fail = "对不起，该代理账号的手机号已在其他微信上绑定。";
									return WxMpXmlOutMessage.TEXT()
											  .content(fail)
											  .fromUser(wxAccountName)   
											  .toUser(wxMpXmlMessage.getFromUserName())
											  .build();
								}
							}
						}else{//没有匹配到正确的号码
							String wrongInput = "对不起,您输入信息有误。您还不能享用代理权利，请先绑定您的代理账号。回复：“##手机号##”即可绑定代理账号，手机号为成为代理时所使用的手机号。如：##13812345678##。";
							return WxMpXmlOutMessage.TEXT()
							  .content(wrongInput)
							  .fromUser(wxAccountName)   
							  .toUser(wxMpXmlMessage.getFromUserName())
							  .build();
						}
						
					}
					
					String content = "对不起您还不能享用代理权利，请先绑定您的代理账号。回复：“##手机号##”即可绑定代理账号，手机号为成为代理时所使用的手机号。如：##13812345678##。";
					return WxMpXmlOutMessage.TEXT()
					  .content(content)
					  .fromUser(wxAccountName)   
					  .toUser(wxMpXmlMessage.getFromUserName())
					  .build();
				}
				
				//点击菜单按钮
				if(wxMpXmlMessage.getMsgType().equalsIgnoreCase("event") && wxMpXmlMessage.getEvent().equalsIgnoreCase(WxConsts.BUTTON_CLICK)){
//					logger.info("click condition!!!");
					if(wxMpXmlMessage.getEventKey().equals("uklm_1_1")){//点击菜单第一个按钮二级菜单的第一个
						WxMpXmlOutNewsMessage.Item item = new WxMpXmlOutNewsMessage.Item();
						item.setDescription("点击查看自己的收支信息");
						item.setPicUrl("http://static.ukejisong.com/image/project/bg-1-home-1800-360-201408291551.jpg");
						item.setTitle("我的对账单");
						item.setUrl(jsSafeDomain + "/balance");
						return WxMpXmlOutMessage.NEWS()
								  .fromUser(wxAccountName)   
								  .toUser(wxMpXmlMessage.getFromUserName())
								  .addArticle(item)
								  .build();
					}
					
					if(wxMpXmlMessage.getEventKey().equals("uklm_1_2")){
						WxMpXmlOutNewsMessage.Item item = new WxMpXmlOutNewsMessage.Item();
						String oauthUrl = wxMpService.oauth2buildAuthorizationUrl(WxConsts.OAUTH2_SCOPE_BASE, "uklm12");
						item.setDescription("点击查看自己生成的优惠码的详情");
						item.setPicUrl("http://static.ukejisong.com/image/project/bg-1-home-1800-360-201408291551.jpg");
						item.setTitle("优惠码详情");
						item.setUrl(oauthUrl);
						logger.info(oauthUrl);
						return WxMpXmlOutMessage.NEWS()
								  .fromUser(wxAccountName)   
								  .toUser(wxMpXmlMessage.getFromUserName())
								  .addArticle(item)
								  .build();
					}
					
					if(wxMpXmlMessage.getEventKey().equals("uklm_2")){
//						WxMpXmlOutNewsMessage.Item item = new WxMpXmlOutNewsMessage.Item();
//						item.setDescription("点击生成优惠码");
//						item.setPicUrl("http://static.ukejisong.com/image/project/bg-1-home-1800-360-201408291551.jpg");
//						item.setTitle("生成优惠码");
//						item.setUrl(jsSafeDomain + "/generate");
//						return WxMpXmlOutMessage.NEWS()
//								  .fromUser(wxAccountName)   
//								  .toUser(wxMpXmlMessage.getFromUserName())
//								  .addArticle(item)
//								  .build();
						String randomCode = RandomUtils.generateUniqueCode(16);
						boolean generateSuccess =  agentCodeInfoService.generateCode(agentInfo, randomCode);
						String content = "";
						if(generateSuccess){
							content = "生成的优惠码：" + randomCode + ",如可访问<a href=\"http://www.ukejisong.com/course/21?acode=" + randomCode + "\">微信课程的优惠链接</a>购买";
						}else{
							content = "生成失败,请稍后重试";
						}
						return WxMpXmlOutMessage.TEXT()
						  .content(content)
						  .fromUser(wxAccountName)
						  .toUser(wxMpXmlMessage.getFromUserName())
						  .build();
					}
				}
				
					
				String content = "恭喜您，收到了我的回复。";
				return WxMpXmlOutMessage.TEXT()
				  .content(content)
				  .fromUser(wxAccountName)   
				  .toUser(wxMpXmlMessage.getFromUserName())
				  .build();
			} 
		};
		
	    WxMpMessageRouter wxMpMessageRouter = new WxMpMessageRouter(wxMpService);
		wxMpMessageRouter.rule()
						 .async(false) 
//						 .msgType(WxConsts.XML_MSG_EVENT)
//						 .event(WxConsts.EVT_SUBSCRIBE)
						 .handler(handler)
						 .end();
		
		
		try {
			WxMpXmlMessage inMessage = WxMpXmlMessage.fromXml(request.getInputStream());
			WxMpXmlOutMessage outMessage = wxMpMessageRouter.route(inMessage);
			return outMessage.toXml();
		} catch (IOException e) {
		}
		return null;
	}
	
	@RequestMapping("/wxredirect")
	public String redirectPage(@RequestParam(value = "code",required = false) String code,@RequestParam(value = "state",required = false) String state,HttpSession httpSession) throws WxErrorException{
		WxMpInMemoryConfigStorage config = new WxMpInMemoryConfigStorage();
		
		config.setAppId(appId);
		config.setSecret(appSecret);
		config.setToken(token);
		
		WxMpServiceImpl wxMpService = new WxMpServiceImpl();
	    wxMpService.setWxMpConfigStorage(config);
	    
	    WxMpOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.oauth2getAccessToken(code);
	    String openId = wxMpOAuth2AccessToken.getOpenId();
//	    WxMpUser wxMpUser = wxMpService.oauth2getUserInfo(wxMpOAuth2AccessToken, null);
		
	    httpSession.setAttribute("agent-open-id", openId);
	    String redirecUrl = "";
	    if(state.equals("uklm12")){
	    	redirecUrl = "code-list";
	    }
	    
		return "redirect:/"+redirecUrl;
	}
}
