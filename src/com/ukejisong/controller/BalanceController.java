package com.ukejisong.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BalanceController {

	@RequestMapping("/balance")
	public String index(HttpSession httpSession){
		return "balance";
	}
}
