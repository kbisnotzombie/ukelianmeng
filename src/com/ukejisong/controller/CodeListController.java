package com.ukejisong.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ukejisong.domain.AgentCodeInfo;
import com.ukejisong.domain.AgentInfo;
import com.ukejisong.orm.PageRequest;
import com.ukejisong.orm.Pagination;
import com.ukejisong.service.AgentCodeInfoService;
import com.ukejisong.service.AgentInfoService;

@Controller
public class CodeListController {
	
	@Resource(name = "agentCodeInfoService")
	public AgentCodeInfoService agentCodeInfoService;
	
	@Resource(name = "agentInfoService")
	public AgentInfoService agentInfoService;
	
	@RequestMapping("/code-list")
	public String index(@RequestParam(value = "page", required = false, defaultValue = "1") int page,@RequestParam(value = "type", required = false, defaultValue = "") String type,Model model,HttpSession httpSession) {
		if(httpSession.getAttribute("agent-open-id") == null){
			return "redirect:/";
		}
		String openId = httpSession.getAttribute("agent-open-id").toString();
		AgentInfo agentInfo = agentInfoService.getAgentInfo(openId);
		int pageSize = 5;
		
		Pagination<AgentCodeInfo> pagination = null;
		if(type.equals("unused")){   
			pagination = agentCodeInfoService.pageAgentCodeInfoByHasUsed(agentInfo.getId(), new PageRequest(page, pageSize), false);
		}else if(type.equals("used")){
			pagination = agentCodeInfoService.pageAgentCodeInfoByHasUsed(agentInfo.getId(), new PageRequest(page, pageSize), true);
		}else{
			pagination = agentCodeInfoService.pageAgentCodeInfo(agentInfo.getId(), new PageRequest(page, pageSize));
		}
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("type", type);
		return "code_list";  
	}
}
