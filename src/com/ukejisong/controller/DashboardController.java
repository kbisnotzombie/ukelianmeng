package com.ukejisong.controller;


import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DashboardController {
	@RequestMapping("/dashboard")
	public String index(HttpSession httpSession) {
//		System.out.println(httpSession.getAttribute("agent-phone"));
		if(httpSession.getAttribute("agent-phone") == null){
			return "redirect:/";
		}
		return "dashboard";  
	}
}
