package com.ukejisong.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ukejisong.domain.AgentInfo;
import com.ukejisong.service.AgentInfoService;

@Controller 
public class HomeController {

	@Resource(name = "agentInfoService")
	private AgentInfoService agentInfoService;
	
	@RequestMapping("")
	public String index(HttpSession httpSession){
//		if(httpSession.getAttribute("agent-phone") != null){
//			return "redirect:/dashboard";
//		}
		return "index";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(HttpSession httpSession, @RequestParam String phone){
		AgentInfo agentInfo = agentInfoService.reerieveAgentInfo(phone);
		
		if(agentInfo == null){
			return "redirect:/";    
		}else{
			httpSession.setAttribute("agent-open-id", agentInfo.getOpenId());
			return "redirect:/code-list";
		}
	}
	
	@RequestMapping(value = "/admin/agent/create", method = RequestMethod.GET)
	public String addAgent(){
		return "create_agent";
	}
	
	@RequestMapping(value = "/admin/agent/create", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> addAgentAccount(@RequestParam(value="phone",required = true) String phone, @RequestParam(value="realname",required = true) String realname, @RequestParam(value="token",required = true) String token){
		Map<String, Object> map = new HashMap<String, Object>();
		if(phone.equals("") || realname.equals("") || token.equals("")){
			map.put("success",false);
			map.put("error", "输入信息不完整");
		}else{
			if(token.equals("Duobei0318")){
				AgentInfo agentInfo = agentInfoService.reerieveAgentInfo(phone);
				
				if(agentInfo == null){
					map.put("success",true);
					agentInfoService.createAgentInfo(realname, phone, "-1");
				}else{
					map.put("success",false);
					map.put("error", "该手机号已经有代理使用，无法创建");
				}
			}else{
				map.put("success",false);
				map.put("error", "token输入错误");
			}
		}
		
		return map;
	}
	
	@RequestMapping(value = "/admin/agent/cancel", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> cancelAgentAccount(@RequestParam(value="phone",required = true) String phone,  @RequestParam(value="token",required = true) String token){
		Map<String, Object> map = new HashMap<String, Object>();
		if(phone.equals("") || token.equals("")){
			map.put("success",false);
			map.put("error", "输入信息不完整");
		}else{
			if(token.equals("Duobei0318")){
				AgentInfo agentInfo = agentInfoService.reerieveAgentInfo(phone);
				
				if(agentInfo == null){
					map.put("success",false);
					map.put("error", "这个手机号没有代理权利");
				}else{
					agentInfoService.updateOpenId("", agentInfo);
					map.put("success",true);
				}
			}else{
				map.put("success",false);
				map.put("error", "token输入错误");
			}
		}
		
		return map;
	}
	
//	@RequestMapping(value = "/logout")
//	public String login(HttpSession httpSession){
//		httpSession.removeAttribute("agent-phone");
//		return "redirect:/";
//	}
	
}
