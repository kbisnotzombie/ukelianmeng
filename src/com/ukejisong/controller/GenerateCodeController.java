package com.ukejisong.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ukejisong.core.utils.RandomUtils;
import com.ukejisong.domain.AgentInfo;
import com.ukejisong.service.AgentCodeInfoService;
import com.ukejisong.service.AgentInfoService;

@Controller
public class GenerateCodeController {
	
	@Resource(name = "agentCodeInfoService")
	private AgentCodeInfoService agentCodeInfoService;
	
	@Resource(name = "agentInfoService")
	private AgentInfoService agentInfoService;
	
	@RequestMapping("/generate")
	public String index(HttpSession httpSession){
		if(httpSession.getAttribute("agent-phone") == null){
			return "redirect:/";
		}
		return "generate_code";
	}
	
	@RequestMapping(value = "/generate", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> generate(HttpSession httpSession){
		String phone = httpSession.getAttribute("agent-phone").toString();
		AgentInfo agentInfo = agentInfoService.reerieveAgentInfo(phone);
		String randomCode = RandomUtils.generateUniqueCode(16);
		
		boolean generateSuccess =  agentCodeInfoService.generateCode(agentInfo, randomCode);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", generateSuccess);
		if(generateSuccess){
			map.put("code", randomCode);
		}
		
		return map;
	}
}
