package com.ukejisong.controller;

import com.alibaba.fastjson.JSON;
import com.ukejisong.weixin.WeixinUtil;

public class WeixinCreateMenu {
	public static String MENU_CREATE = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN";
	//需要更新access_token
	public static String ACCESS_TOKEN = "7z3hCqbinH-krnEEKN5fdB3BD_fZZTPFUUM5hhijqkXUD_uVKO2AERXzafWqSaBgIvDu6TdhxLQGiGbnp1w69qn5iNrjEd4TlGD8Gg7zkOo";
	
	public String CreateMenu(String jsonMenu) {  
        String resultStr = "";  
        // 调用接口获取token  
        String token = ACCESS_TOKEN;  
        if (token != null) {  
            // 调用接口创建菜单  
            int result = createMenu(jsonMenu, token);  
            // 判断菜单创建结果  
            if (0 == result) {  
                resultStr = "菜单创建成功";  
//                log.info(resultStr);  
            } else {  
                resultStr = "菜单创建失败，错误码：" + result;  
//                log.error(resultStr);  
            }  
        }  
  
        return resultStr;  
    }
	
	public static int createMenu(String jsonMenu, String accessToken) {  
        // 拼装创建菜单的url  
        String url = MENU_CREATE.replace("ACCESS_TOKEN", accessToken);  
        // 调用接口创建菜单  
        String t = WeixinUtil.httpRequest(url, "POST", jsonMenu);
  
        return JSON.parseObject(t).getIntValue("errcode");  
    }
	
	public static void main(String[] args) {  
        // 这是一个符合菜单的json格式，“\”是转义符  
        String jsonMenu = "{\"button\":[{\"name\":\"查询\",\"sub_button\":[{\"type\":\"click\",\"name\":\"对账单\",\"key\":\"uklm_1_1\"},{\"type\":\"click\",\"name\":\"优惠码\",\"key\":\"uklm_1_2\"}]},{\"name\":\"生成码\",\"type\":\"click\",\"key\":\"uklm_2\"}]}";  
        WeixinCreateMenu impl = new WeixinCreateMenu();  
        impl.CreateMenu(jsonMenu);  
    } 
}



//uklm as:3cf117bc5a4d15ae05c52f845ea31d33