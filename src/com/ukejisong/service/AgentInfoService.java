package com.ukejisong.service;

import com.ukejisong.domain.AgentInfo;

public interface AgentInfoService {
	AgentInfo reerieveAgentInfo(String phone);
	
	AgentInfo getAgentInfo(String openId);
	
	void updateOpenId(String openId,AgentInfo agentInfo);
	
	void createAgentInfo(String realname,String phone, String parentAgentId);
	
}
