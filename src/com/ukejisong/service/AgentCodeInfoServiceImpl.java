package com.ukejisong.service;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ukejisong.dao.AgentCodeInfoDAO;
import com.ukejisong.domain.AgentCodeInfo;
import com.ukejisong.domain.AgentInfo;
import com.ukejisong.orm.PageRequest;
import com.ukejisong.orm.PageRequest.Sort;
import com.ukejisong.orm.Pagination;

@Service("agentCodeInfoService")
@Transactional
public class AgentCodeInfoServiceImpl implements AgentCodeInfoService{

	@Resource(name = "agentCodeInfoDAO")
	private AgentCodeInfoDAO agentCodeInfoDAO;
	
	@Override
	public boolean generateCode(AgentInfo agentInfo, String code) {
		if(agentInfo != null && code != null){
			AgentCodeInfo agentCodeInfo = new AgentCodeInfo();
			agentCodeInfo.setAgentId(agentInfo.getId());
			agentCodeInfo.setCode(code);
			agentCodeInfo.setDisabled(false);
			agentCodeInfo.setHasUsed(false);
			agentCodeInfo.setLastModifyTime(new Date());
			agentCodeInfoDAO.save(agentCodeInfo);
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Pagination<AgentCodeInfo> pageAgentCodeInfo(Long agentId,
			PageRequest pageRequest) {
		pageRequest.setOrderBy("lastModifyTime");
		pageRequest.setOrderDir(Sort.DESC); 
		Pagination<AgentCodeInfo> page = agentCodeInfoDAO.pageByAgentId(agentId, pageRequest);
		return page;
	}

	@Override
	public Pagination<AgentCodeInfo> pageAgentCodeInfoByHasUsed(Long agentId,
			PageRequest pageRequest, boolean hasUsed) {
		pageRequest.setOrderBy("lastModifyTime");
		pageRequest.setOrderDir(Sort.DESC); 
		Pagination<AgentCodeInfo> page = agentCodeInfoDAO.pageByAgentIdAndHasUsed(agentId, pageRequest, hasUsed);
		return page;
	}

}
