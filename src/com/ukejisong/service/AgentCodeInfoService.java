package com.ukejisong.service;

import com.ukejisong.domain.AgentCodeInfo;
import com.ukejisong.domain.AgentInfo;
import com.ukejisong.orm.PageRequest;
import com.ukejisong.orm.Pagination;

public interface AgentCodeInfoService {
	public boolean generateCode(AgentInfo agentInfo,String code);
	
	public Pagination<AgentCodeInfo> pageAgentCodeInfo(Long agentId, PageRequest pageRequest);
	
	public Pagination<AgentCodeInfo> pageAgentCodeInfoByHasUsed(Long agentId, PageRequest pageRequest, boolean hasUsed);
}
