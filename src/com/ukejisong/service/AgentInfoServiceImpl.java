package com.ukejisong.service;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ukejisong.dao.AgentInfoDAO;
import com.ukejisong.domain.AgentInfo;

@Service("agentInfoService")
@Transactional
public class AgentInfoServiceImpl implements AgentInfoService{

	@Resource(name = "agentInfoDAO")
	private AgentInfoDAO agentInfoDAO;
	
	@Override
	public AgentInfo reerieveAgentInfo(String phone) {
		AgentInfo agentInfo = agentInfoDAO.findByPhone(phone);
		return agentInfo;
	}

	@Override
	public AgentInfo getAgentInfo(String openId) {
		AgentInfo agentInfo = agentInfoDAO.findByOpenId(openId);
		return agentInfo;
	}

	@Override
	public void updateOpenId(String openId, AgentInfo agentInfo) {
		agentInfo.setOpenId(openId);
		agentInfo.setLastModifyTime(new Date());
		agentInfoDAO.update(agentInfo);
	}

	@Override
	public void createAgentInfo(String realname, String phone, String parentAgentId) {
		AgentInfo agentInfo = new AgentInfo();
		agentInfo.setCreateTime(new Date());
		agentInfo.setDisabled(false);
		agentInfo.setLastModifyTime(new Date());
		agentInfo.setNickname(realname);
		agentInfo.setPhone(phone);
		agentInfo.setOpenId("");
		agentInfo.setParentAgentId(parentAgentId);
		agentInfoDAO.save(agentInfo);
	}

}
