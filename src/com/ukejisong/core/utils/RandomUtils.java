package com.ukejisong.core.utils;

import java.util.UUID;

public class RandomUtils {
	
	public static String generateUniqueCode(int length) {
		String randomString = UUID.randomUUID().toString().replace("-", "").substring(0, length);
		return randomString;
	}
	
	public static void main(String[] args) {
		System.out.println(generateUniqueCode(10));
	}
}
