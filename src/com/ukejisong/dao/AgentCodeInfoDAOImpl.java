package com.ukejisong.dao;

import org.springframework.stereotype.Repository;

import com.ukejisong.domain.AgentCodeInfo;
import com.ukejisong.orm.PageRequest;
import com.ukejisong.orm.Pagination;

@Repository("agentCodeInfoDAO")
public class AgentCodeInfoDAOImpl extends AbsHibernateGenericDAO<AgentCodeInfo, Long> implements AgentCodeInfoDAO{

	@Override
	public Pagination<AgentCodeInfo> pageByAgentId(Long agentId,
			PageRequest pageRequest) {
		String hql = "from AgentCodeInfo where agentId = ? and disabled = false";
		return pageByHql(pageRequest, hql, agentId);
	}

	@Override
	public Pagination<AgentCodeInfo> pageByAgentIdAndHasUsed(Long agentId,
			PageRequest pageRequest, boolean hasUsed) {
		String hql = "from AgentCodeInfo where agentId = ? and hasUsed = ? and disabled = false";
		return pageByHql(pageRequest, hql, agentId, hasUsed);
	}

}
