package com.ukejisong.dao;

import com.ukejisong.domain.AgentInfo;

public interface AgentInfoDAO extends HibernateDAO<AgentInfo, Long>{
	AgentInfo findByPhone(String phone);
	
	AgentInfo findByOpenId(String openId);
	
	
}
