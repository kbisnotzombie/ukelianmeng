package com.ukejisong.dao;

import com.ukejisong.domain.AgentCodeInfo;
import com.ukejisong.orm.PageRequest;
import com.ukejisong.orm.Pagination;

public interface AgentCodeInfoDAO extends HibernateDAO<AgentCodeInfo, Long>{
	public Pagination<AgentCodeInfo> pageByAgentId(Long agentId, PageRequest pageRequest);
	
	public Pagination<AgentCodeInfo> pageByAgentIdAndHasUsed(Long agentId, PageRequest pageRequest,boolean hasUsed);
}
