package com.ukejisong.dao;

import org.springframework.stereotype.Repository;

import com.ukejisong.domain.AgentInfo;

@Repository("agentInfoDAO")
public class AgentInfoDAOImpl extends AbsHibernateGenericDAO<AgentInfo, Long> implements AgentInfoDAO{

	@Override
	public AgentInfo findByPhone(String phone) {
		String hql = "from AgentInfo where phone = ? and disabled = false";
		return findUnique(hql, phone);
	}

	@Override
	public AgentInfo findByOpenId(String openId) {
		String hql = "from AgentInfo where openId = ? and disabled = false";
		return findUnique(hql, openId);
	}

}
