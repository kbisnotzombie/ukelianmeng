# /bin/sh

./gradlew war

/etc/init.d/tomcatd stop

rm /usr/local/tomcat/default/webapps/ROOT.war
rm -rf /usr/local/tomcat/default/webapps/ROOT

cp ./build/libs/ROOT.war /usr/local/tomcat/default/webapps/ROOT.war

/etc/init.d/tomcatd start
