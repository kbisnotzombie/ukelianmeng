<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="include/taglib.jsp" %>
<%@ include file="include/namespace.jsp" %>
<!DOCTYPE html>
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">
	<title>Insert title here</title>
	</head>
	<body>
		<h1>添加新的代理</h1>
		<form id="form" action="${pageContext.request.contextPath}/admin/agent/create" method="post">
			<div>请输入新代理的手机号：</div> 
			<input type="tel" placeholder="13800000000" value="" name="phone"/>
			<div>请输入新代理姓名：</div> 
			<input type="text" placeholder="张三" value="" name="realname"/>
			<div>请输入权限token：</div> 
			<input type="text" placeholder="" value="" name="token"/>
			<input type="submit" value="生成"/>
		</form>
		
		<h1>取消代理和微信的绑定</h1>
		<form id="cancel_form" action="${pageContext.request.contextPath}/admin/agent/cancel" method="post">
			<div>请输入代理的手机号：</div> 
			<input type="tel" placeholder="13800000000" value="" name="phone"/>
			<div>请输入权限token：</div> 
			<input type="text" placeholder="" value="" name="token"/>
			<input type="submit" value="确认"/>
		</form>
		
		<script src="${js}/jquery-1.10.1.min.js"></script>
		<script type="text/javascript">
			$(function(){
				$('#form').submit(function(){
					$.ajax({
						async:false,
						url:$('#form').attr('action'),
						type:'post',
						dataType:'json', 
						data:$('#form').serialize(),
						context:$(this),
						beforeSend: function (xhr) {
					        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
					    },
						success:function(json){
							if(json.success){
								alert('生成成功');
							}else{
								alert(json.error);
							}
						}
					});
					return false;
				});
				
				$('#cancel_form').submit(function(){
					$.ajax({
						async:false,
						url:$('#cancel_form').attr('action'),
						type:'post',
						dataType:'json', 
						data:$('#cancel_form').serialize(),
						context:$(this),
						beforeSend: function (xhr) {
					        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
					    },
						success:function(json){
							if(json.success){
								alert('取消成功');
							}else{
								alert(json.error);
							}
						}
					});
					return false;
				});
			});
		</script>
	</body>
</html>