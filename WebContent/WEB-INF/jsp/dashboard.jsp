<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="include/taglib.jsp" %>
<%@ include file="include/namespace.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">
	<title>Insert title here</title>
	</head>
	<body>
		<h1>代理系统控制台</h1> 
		<div style="margin-top:20px;"><a href="${pageContext.request.contextPath}/code-list">查看记录</a></div>
		<div style="margin-top:20px;"><a href="${pageContext.request.contextPath}/generate">生成优惠码</a></div>
		<div style="margin-top:20px;"><a href="${pageContext.request.contextPath}/logout">退出当前代理账号</a></div> 
	</body>
</html>