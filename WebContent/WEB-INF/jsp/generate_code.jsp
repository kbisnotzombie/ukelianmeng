<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="include/taglib.jsp" %>
<%@ include file="include/namespace.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">
	<title>Insert title here</title>
	<style type="text/css">
		input{
			display: block;
			width:100%;
		}
	</style>
	</head>
	<body>
		<h1>代理系统生成优惠码</h1> 
		<div class="result"></div>
		<div style="margin-top:20px;"><a class="generate_btn" href="javascript:;">生成优惠码</a></div>
		<div style="margin-top:20px;"><a class="" href="${pageContext.request.contextPath}/dashboard">返回控制台</a></div>
		
		<input type="hidden" id="root_url" value="${pageContext.request.contextPath}"/>
		<script src="${js}/jquery-1.10.1.min.js"></script>
		<script type="text/javascript">
			$(function(){
				$('.generate_btn').click(function(){
					$.ajax({
						async:false,
						url:$('#root_url').val() + '/generate',
						type:'post',
						dataType:'json',
						context:$(this),
						/* data:{
							alipayAccount:$updateInput.val()
						}, */
						beforeSend: function (xhr) {
							xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
						},
						success:function(json) {
							if(json.success){
								/* alert('生成成功'); */
								$('.result').html('优惠码（一个优惠码仅供一人使用一次）:' + json.code + '。如优惠链接(复制发给别人)：<input type="text" value="http://www.ukejisong.com/course/21?acode=' + json.code + '"/>');
							}
							else{
								alert('生成失败，请重新尝试');
							}
						}
					});
				});
			});
		</script>
	</body>
</html>