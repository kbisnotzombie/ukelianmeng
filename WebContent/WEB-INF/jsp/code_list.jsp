<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="include/taglib.jsp" %>
<%@ include file="include/namespace.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">
	<title>Insert title here</title>
	</head>
	<body>
		<h1>代理系统查看记录</h1> 
		<div style="margin-top:20px;display:none;"><a class="" href="${pageContext.request.contextPath}/dashboard">返回控制台</a></div>
		<div style="margin:10px 0;">分类查询 <a href="${pageContext.request.contextPath}/code-list">全部优惠码</a> <a href="${pageContext.request.contextPath}/code-list?type=unused">未使用的优惠码</a> <a href="${pageContext.request.contextPath}/code-list?type=used">已使用的优惠码</a> <a href="javascript:;" onclick="alert('正在开发中');">成功交易的优惠码</a></div>
		<table>
			<tr>
				<th>优惠码</th>
				<th>状态</th>
				<!-- <th>提成金额</th> -->
				<th>生成时间</th>
				<th>备注</th>
			</tr>
			<c:forEach items="${pagination.items}" var="codeInfo">
				<tr>
					<td>${codeInfo.code}</td>
					<td><c:choose><c:when test="${codeInfo.hasUsed}">已使用</c:when><c:otherwise>未使用</c:otherwise></c:choose></td>
					<!-- <td>暂无</td> -->
					<td><fmt:formatDate value="${codeInfo.lastModifyTime}" pattern="yyyy-MM-dd HH:mm"/></td>
					<td>暂无</td>
				</tr>
			</c:forEach>
			<tr>
			</tr>
		</table>
		
		<c:if test="${pagination.totalPages > 1}"> 
			<c:choose>
				<c:when test="${type == 'unused'}"><c:set var="pageUrl" value="${pageContext.request.contextPath}/code-list?type=unused&"/></c:when>
				<c:when test="${type == 'used'}"><c:set var="pageUrl" value="${pageContext.request.contextPath}/code-list?type=used&"/></c:when>
				<c:otherwise><c:set var="pageUrl" value="${pageContext.request.contextPath}/code-list?"/></c:otherwise>
			</c:choose>
			
			<div class="page_turn">
				<c:if test="${pagination.hasPrePage}">
					<a class="pre_page" <c:choose><c:when test="${pagination.prePage == 1}">href="${pageUrl}"</c:when><c:otherwise>href="${pageUrl}page=${pagination.prePage}"</c:otherwise></c:choose> title="上一页">上一页</a>
				</c:if>
				<c:forEach items="${pagination.slider}" var="slider">
		        	<a <c:choose><c:when test="${slider == 1}">href="${pageUrl}"</c:when><c:otherwise>href="${pageUrl}page=${slider}"</c:otherwise></c:choose>  title="第${slider}页" <c:if test="${pagination.pageNo eq slider}">class="current"</c:if>>${slider}</a>
	          	</c:forEach>
				<c:if test="${pagination.hasNextPage}">
					<a class="next_page" rel="nofollow" href="${pageUrl}page=${pagination.nextPage}" title="下一页">下一页</a>
				</c:if>
			</div>
		</c:if>
	</body>
</html>